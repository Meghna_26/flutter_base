import 'package:base_theme/helpers/Common.dart';
import 'package:base_theme/widgets/common/menu_page.dart';
import 'package:base_theme/widgets/common/zoom_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'common/common_bottom_menu.dart';

class OpenFlutterScaffold extends StatefulWidget {
  final Color background;
  final String title;
  final Widget body;
  final int bottomMenuIndex;
  final List<String> tabBarList;
  final TabController tabController;
  final bool isDrawer;

  const OpenFlutterScaffold({
    Key key,
    this.background,
    @required this.title,
    @required this.body,
    this.bottomMenuIndex,
    this.tabBarList,
    this.tabController,
    this.isDrawer,
  }) : super(key: key);

  @override
  _OpenFlutterScaffoldState createState() => _OpenFlutterScaffoldState();
}

class _OpenFlutterScaffoldState extends State<OpenFlutterScaffold>
    with TickerProviderStateMixin {
  MenuController menuController;

  @override
  void initState() {
    super.initState();
    menuController = new MenuController(
      vsync: this,
    )..addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    var tabBars = <Tab>[];
    var _theme = Theme.of(context);
    if (widget.tabBarList != null) {
      for (var i = 0; i < widget.tabBarList.length; i++) {
        tabBars.add(Tab(key: UniqueKey(), text: widget.tabBarList[i]));
      }
    }
    Widget tabWidget = tabBars.isNotEmpty
        ? TabBar(
            unselectedLabelColor: Common.accentColor,
            unselectedLabelStyle: TextStyle(
              fontWeight: FontWeight.normal,
              color: Common.accentColor,
              fontSize: 22,
            ),
            labelColor: Common.accentColor,
            labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              color: Common.accentColor,
              fontSize: 22,
            ),
            tabs: tabBars,
            controller: widget.tabController,
            indicatorColor: _theme.accentColor,
            indicatorSize: TabBarIndicatorSize.tab)
        : null;

    Drawer drawer = Drawer(
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.85,
              child: DrawerHeader(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/profile/user-profile.jpeg"),
                        fit: BoxFit.cover)),
                child: Text("Header"),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: ListView(children: [
              ListTile(
                title: Text("Home1"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home2"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home3"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home4"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home5"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home6"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home7"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Text("Home8"),
                onTap: () {
                  Navigator.of(context).pop();
                },
              )
            ]),
          )
        ],
      ),
    );

     return ChangeNotifierProvider(
       create: (context) => menuController,
      child: widget.isDrawer
          ? ZoomScaffold(
              contentScreen: Layout(
                contentBuilder: (cc) => Container(
                  color: Colors.grey[200],
                  child: Container(
                    color: Colors.grey[200],
                  ),
                ),
              ),
              menuScreen: MenuScreen(),
            )
          : Scaffold(
              backgroundColor: widget.background,
              appBar: widget.title != null
                  ? AppBar(
                      title: Text(
                        widget.title,
                      ),
                      bottom: tabWidget,
                      actions: <Widget>[
                          Row(children: <Widget>[
                            Icon(Icons.share),
                          ])
                        ])
                  : null,
              drawer: drawer,
              body: widget.body,
              bottomNavigationBar: CommonBottomMenu(widget.bottomMenuIndex),
            ),
    );
   /* return ChangeNotifierProvider(
      builder: (context) =>  menuController,
      child: ZoomScaffold(
        contentScreen: Layout(
          contentBuilder: (cc) => Container(
            color: Colors.grey[200],
            child: Container(
              color: Colors.grey[200],
            ),
          ),
        ),
        menuScreen: MenuScreen(),
      ),
    );*/
  }
}
