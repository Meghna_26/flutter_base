import 'package:flutter/material.dart';

class Common{
  static const Color primaryColor = Colors.blue;
  static const Color accentColor = Colors.black;
  static const Color background = Colors.white;

  static const home = '/';
  static const shop = 'shop';
  static const cart = 'cart';
  static const favourites = 'favourites';
}

class CommonTheme{
  static ThemeData of(context) {
    var theme = Theme.of(context);
    return theme.copyWith(
      accentColor: Common.accentColor,
      primaryColor: Common.primaryColor,
      appBarTheme: theme.appBarTheme.copyWith(
        color: Common.primaryColor,
      )
    );
 }
}