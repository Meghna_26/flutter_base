//global
import 'package:flutter/widgets.dart';
//local
import 'package:base_theme/screens/home_screen.dart';

class Routes {
  static Map<String, WidgetBuilder> getAll() {
    return {
      "/": (context) => HomeScreen(),
    };
  }
}
