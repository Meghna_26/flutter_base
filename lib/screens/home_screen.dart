import 'package:base_theme/helpers/Common.dart';
import 'package:base_theme/widgets/open_flutter_scaffod.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin{
  TabController _tabController;
  final List<String> types = ['new', 'old'];

  @override
  void initState() {
    _tabController = TabController(
      length: types.length,
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OpenFlutterScaffold(
      title: 'title',
      body: null,
      background: Common.background,
      tabBarList: types,
      tabController: _tabController,
      bottomMenuIndex: 3 ,
      isDrawer: false,
    );
  }
}
